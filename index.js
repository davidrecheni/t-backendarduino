"use strict";

const five = require("johnny-five"),
    app = require('express')(),
    port = 8000,
    http = require('http').createServer(app),
    configs = five.Motor.SHIELD_CONFIGS.ADAFRUIT_V2

const io = require('socket.io')(http, {
    cors: {
        "origin": "*",
        "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
        "optionsSuccessStatus": 204
    }
})
// "preflightContinue": false,
let led,
    motor1,
    motor2

let queue = []

let currentPlayer,
    isPlaying = false

const GAME_LENGHT = 100000,
    READY_WAIT = 50000

const removeFromQueue = (arr, item) => {
    const index = arr.findIndex(e => e.id = item);
    if (index !== -1) {
        arr.splice(index, 1);
    } else {
        console.log('Notfound', arr, item)
    }
}

io.on('connection', (socket) => {

    const timeUp = () => {
        motor1.stop()
        motor2.stop()
        if (queue.length) { // && isPlaying?
            io.to(queue[0].id).emit('stopPlaying', true);
            isPlaying = false
            if (queue.length > 1) removeFromQueue(queue, socket.id)
            console.log('Time up', queue)
            startGame()
        } else {
            isPlaying = false
            console.log('No players waiting')
        }
    }

    const startGame = () => {
        if (queue.length) {
            io.to(queue[0].id).emit('startPlaying', true);
            io.emit('currentPlayer', queue[0].name)
            isPlaying = true
            currentPlayer = queue[0]
            console.log(queue[0], ' is now playing.')
            setTimeout(() => timeUp(), GAME_LENGHT)
        } else {
            isPlaying = false
            console.log('No players waiting')
        }
    }

    const isPlayerReady = () => {
        if (queue.length) {
            io.to(queue[0].id).emit('isReady', true);
            setTimeout(() => {
                if (!isPlaying) timeUp()
            }, READY_WAIT)
        } else {
            isPlaying = false
            console.log('No players waiting')
        }
    }

    if (isPlaying && currentPlayer) {
        io.to(socket.id).emit('currentPlayer', currentPlayer.name);
    }

    socket.on('joinQueue', (playerName) => {
        queue.push({ id: socket.id, name: playerName })
        io.emit('queueStatus', { next: queue[0], size: queue.length })
        if (!isPlaying) isPlayerReady()
        console.log('queue updated', queue)
    })

    socket.on('imReady', () => {
        if (!isPlaying) { startGame() }
    })

    socket.on('queueStatus', _ => {
        queue.push(socket.id)
        console.log('queue updated', queue)
    })

    socket.on('startTimer', _ => {
        console.log('timer started')
        setTimeout(() => timeUp(), GAME_LENGHT)
    })

    const scale = (num, in_min, in_max, out_min, out_max) => {
        return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    const limitMvmt = (axisData) => {
        if (axisData < -30) {
            return -30
        } else if (axisData > 30) {
            return 30
        } else {
            return axisData.toFixed(3)
        }
    }

    const calculateMvmt = (mvmt, motor) => {
        const scaledMvmt = scale(limitMvmt(mvmt), -30, 30, -255, 255)
        if (scaledMvmt > 0) {
            motor.fwd(scaledMvmt)
        } else if (scaledMvmt < 0) {
            motor.rev(Math.abs(scaledMvmt))
        } else {
            motor.stop()
        }
        return scaledMvmt
    }

    let mvmt1 = 0,
        mvmt2 = 0

    socket.on('movement', data => {
        // Gyro.y == left right
        // Gyro.x == up down
        mvmt1 = calculateMvmt(data.magne.y, motor2)
        mvmt2 = calculateMvmt(data.magne.x, motor1)
        console.log(`movement received: ${mvmt1}  ${mvmt2}`)
    })

    socket.on('disconnect', () => {
        removeFromQueue(queue, socket.id)
        io.emit('queueStatus', { next: queue[0], size: queue.length })
        if (currentPlayer && currentPlayer.id === socket.id) isPlayerReady()
        console.log('user disconnected');
    });

});

const boardModule = require('./src/board')

boardModule.initBoard().then(_ => {
    led = new five.Led(13);

    try {
        motor1 = new five.Motor(configs.M3)
        motor2 = new five.Motor(configs.M4)
    } catch (e) {
        console.log(`Error!: ${e}`)
    }
    console.log("### Board ready!");

})

app.get('/update-sensor:status', function (req, res) {

    console.log('Sensor status update:', status)
});

app.get('/led/:mode', function (req, res) {
    if (led) {
        var status = "OK";
        switch (req.params.mode) {
            case "on":
                led.on();
                break;
            case "off":
                led.off();
                break;
            case "blink":
                led.blink();
                break;
            case "stop":
                led.stop();
                break;
            default:
                status = "Unknown: " + req.params.mode;
                break;
        }
        console.log(status);
        res.send(status);
    } else {
        res.send('Board NOT ready!')
    }
});


http.listen(port, function () {
    console.log('Listening on port2 ' + port);
});