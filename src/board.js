
module.exports = {
    initBoard: initBoard
};

const five = require('johnny-five'),
    Q = require('q');

function initBoard() {
    const deferred = Q.defer()
    // const board = new five.Board({ port: "COM3" });
    const board = new five.Board({ port: "/dev/tty.usbmodem142101" });


    board.on('ready', function() {
        deferred.resolve();
    });

    return deferred.promise;
}